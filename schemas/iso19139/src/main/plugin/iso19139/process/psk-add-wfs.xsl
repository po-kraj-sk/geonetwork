<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright (C) 2001-2016 Food and Agriculture Organization of the
  ~ United Nations (FAO-UN), United Nations World Food Programme (WFP)
  ~ and United Nations Environment Programme (UNEP)
  ~
  ~ This program is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; either version 2 of the License, or (at
  ~ your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful, but
  ~ WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  ~ General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
  ~
  ~ Contact: Jeroen Ticheler - FAO - Viale delle Terme di Caracalla 2,
  ~ Rome - Italy. email: geonetwork@osgeo.org
  -->

<!--
Stylesheet used to update metadata to improve validation.
https://taskman.eionet.europa.eu/issues/104851

Restore a backup
UPDATE metadata a SET data = (SELECT data FROM metadata20181010 b WHERE a.uuid= b.uuid);
-->
<xsl:stylesheet xmlns:gmd="http://www.isotc211.org/2005/gmd"
                xmlns:gco="http://www.isotc211.org/2005/gco"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:srv="http://www.isotc211.org/2005/srv"
                xmlns:gmx="http://www.isotc211.org/2005/gmx"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:util="java:org.fao.geonet.util.XslUtil"
                xmlns:skos="http://www.w3.org/2004/02/skos/core#"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:geonet="http://www.fao.org/geonetwork"
                exclude-result-prefixes="#all"
                version="2.0">

  <xsl:output indent="yes"/>

  <xsl:variable name="uuid"
                select="//gmd:fileIdentifier/gco:CharacterString"/>


  <xsl:template match="//gmd:transferOptions/gmd:MD_DigitalTransferOptions">

    <xsl:copy>
      <xsl:apply-templates select="@*|node()|comment()"/>
      <xsl:if
        test="count(gmd:onLine/gmd:CI_OnlineResource[starts-with(gmd:protocol/gco:CharacterString, 'OGC:WMS')]) > 0">
        <xsl:variable name="nodes"
                      select="gmd:onLine/gmd:CI_OnlineResource[starts-with(gmd:protocol/gco:CharacterString, 'OGC:WMS')]"></xsl:variable>
        <xsl:variable name="layerName" select="$nodes[1]/gmd:name/gco:CharacterString"></xsl:variable>
        <xsl:variable name="wmsUrl" select="concat(substring-before($nodes[1]/gmd:linkage/gmd:URL, '/wms'), '/wfs')"></xsl:variable>
        <xsl:if
          test="count(gmd:onLine/gmd:CI_OnlineResource[starts-with(gmd:protocol/gco:CharacterString, 'OGC:WFS')]) = 0">
          <gmd:onLine>
            <gmd:CI_OnlineResource>
              <gmd:linkage>
                <gmd:URL><xsl:value-of select="$wmsUrl"></xsl:value-of> </gmd:URL>
              </gmd:linkage>
              <gmd:protocol>
                <gco:CharacterString>OGC:WFS</gco:CharacterString>
              </gmd:protocol>
              <gmd:name>
                <gco:CharacterString><xsl:value-of select="$layerName"></xsl:value-of> </gco:CharacterString>
              </gmd:name>
              <gmd:description>
                <gco:CharacterString>Download</gco:CharacterString>
              </gmd:description>
              <gmd:function>
                <gmd:CI_OnLineFunctionCode codeList="http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources/codelist/ML_gmxCodelists.xml#CI_OnLineFunctionCode"
                                           codeListValue="download"/>
              </gmd:function>
            </gmd:CI_OnlineResource>
          </gmd:onLine>

        </xsl:if>
      </xsl:if>
    </xsl:copy>
  </xsl:template>


  <!-- Do a copy of every nodes and attributes -->
  <xsl:template match="@*|node()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()|comment()"/>
    </xsl:copy>

  </xsl:template>

  <!-- Remove geonet:* elements. -->
  <xsl:template match="geonet:*" priority="2"/>

</xsl:stylesheet>
