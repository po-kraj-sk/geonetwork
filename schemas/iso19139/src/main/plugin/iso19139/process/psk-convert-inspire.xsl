<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright (C) 2001-2016 Food and Agriculture Organization of the
  ~ United Nations (FAO-UN), United Nations World Food Programme (WFP)
  ~ and United Nations Environment Programme (UNEP)
  ~
  ~ This program is free software; you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation; either version 2 of the License, or (at
  ~ your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful, but
  ~ WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  ~ General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program; if not, write to the Free Software
  ~ Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301, USA
  ~
  ~ Contact: Jeroen Ticheler - FAO - Viale delle Terme di Caracalla 2,
  ~ Rome - Italy. email: geonetwork@osgeo.org
  -->

<!--
Stylesheet used to update metadata to improve validation.
https://taskman.eionet.europa.eu/issues/104851

Restore a backup
UPDATE metadata a SET data = (SELECT data FROM metadata20181010 b WHERE a.uuid= b.uuid);
-->
<xsl:stylesheet xmlns:gmd="http://www.isotc211.org/2005/gmd"
                xmlns:gco="http://www.isotc211.org/2005/gco"
                xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                xmlns:srv="http://www.isotc211.org/2005/srv"
                xmlns:gmx="http://www.isotc211.org/2005/gmx"
                xmlns:xlink="http://www.w3.org/1999/xlink"
                xmlns:util="java:org.fao.geonet.util.XslUtil"
                xmlns:skos="http://www.w3.org/2004/02/skos/core#"
                xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:geonet="http://www.fao.org/geonetwork"
                exclude-result-prefixes="#all"
                version="2.0">

  <xsl:output indent="yes"/>

  <xsl:variable name="uuid"
                select="//gmd:fileIdentifier/gco:CharacterString"/>

  <xsl:template match="gmd:MD_Metadata">
    <xsl:element name="{name()}" namespace="{namespace-uri()}">
      <xsl:copy-of select="namespace::*[not(name() = 'gse') and not(name() = 'gml')]" />
      <xsl:namespace name="gmx" select="'http://www.isotc211.org/2005/gmx'"/>
      <xsl:namespace name="gml" select="'http://www.opengis.net/gml/3.2'"/>

      <!-- Fixed value for schemaLocation -->
      <xsl:attribute name="xsi:schemaLocation">http://www.isotc211.org/2005/gmd https://inspire.ec.europa.eu/draft-schemas/inspire-md-schemas-temp/apiso-inspire/apiso-inspire.xsd</xsl:attribute>
      <xsl:apply-templates select="@*[name() != 'xsi:schemaLocation']"/>

      <xsl:apply-templates select="gmd:fileIdentifier" />
      <xsl:apply-templates select="gmd:language" />
      <xsl:apply-templates select="gmd:characterSet" />
      <xsl:apply-templates select="gmd:parentIdentifier" />
      <xsl:apply-templates select="gmd:hierarchyLevel" />
      <xsl:apply-templates select="gmd:hierarchyLevelName" />

      <!-- Add required gmd:hierarchyLevelName for services if missing -->
      <xsl:if test="not(gmd:hierarchyLevel) and count(//srv:SV_ServiceIdentification) = 0">
        <gmd:hierarchyLevel>
          <gmd:MD_ScopeCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_ScopeCode" codeListValue="dataset">dataset</gmd:MD_ScopeCode>
        </gmd:hierarchyLevel>
      </xsl:if>

      <xsl:apply-templates select="gmd:contact" />
      <xsl:apply-templates select="gmd:dateStamp" />
      <xsl:apply-templates select="gmd:metadataStandardName" />
      <xsl:apply-templates select="gmd:metadataStandardVersion" />
      <xsl:apply-templates select="gmd:dataSetURI" />
      <xsl:apply-templates select="gmd:locale" />
      <xsl:apply-templates select="gmd:spatialRepresentationInfo" />
      <xsl:apply-templates select="gmd:referenceSystemInfo" />
      <xsl:apply-templates select="gmd:metadataExtensionInfo" />
      <xsl:apply-templates select="gmd:identificationInfo" />
      <xsl:apply-templates select="gmd:contentInfo" />
      <xsl:apply-templates select="gmd:distributionInfo" />
      <xsl:apply-templates select="gmd:dataQualityInfo" />
      <xsl:apply-templates select="gmd:portrayalCatalogueInfo" />
      <xsl:apply-templates select="gmd:metadataConstraints" />
      <xsl:apply-templates select="gmd:applicationSchemaInfo" />
      <xsl:apply-templates select="gmd:metadataMaintenance" />
      <xsl:apply-templates select="gmd:series" />
      <xsl:apply-templates select="gmd:describes" />
      <xsl:apply-templates select="gmd:propertyType" />
      <xsl:apply-templates select="gmd:featureType" />
      <xsl:apply-templates select="gmd:featureAttribute" />
    </xsl:element>
  </xsl:template>

  <!--Remove all gmd:resourceConstraints and add these 2 ones-->
  <xsl:template match="gmd:resourceConstraints" priority="2">
    <xsl:if test="preceding-sibling::*[1]/name() != 'gmd:resourceConstraints'">
      <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints>
          <gmd:useConstraints>
            <gmd:MD_RestrictionCode
              codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xmlMD_RestrictionCode"
              codeListValue="otherRestrictions"/>
          </gmd:useConstraints>
          <gmd:otherConstraints>
            <gco:CharacterString>Creative Commons Attribution 4.0 International (CC BY 4.0)</gco:CharacterString>
          </gmd:otherConstraints>
        </gmd:MD_LegalConstraints>
      </gmd:resourceConstraints>
      <gmd:resourceConstraints>
        <gmd:MD_LegalConstraints>
          <gmd:accessConstraints>
            <gmd:MD_RestrictionCode
              codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_RestrictionCode"
              codeListValue="otherRestrictions">otherRestrictions
            </gmd:MD_RestrictionCode>
          </gmd:accessConstraints>
          <gmd:otherConstraints>
            <gmx:Anchor
              xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez
              obmedzenia prístupu
            </gmx:Anchor>
          </gmd:otherConstraints>
        </gmd:MD_LegalConstraints>
      </gmd:resourceConstraints>
    </xsl:if>
  </xsl:template>

  <xsl:template match="gmd:dataQualityInfo" priority="2">
    <gmd:dataQualityInfo>
      <gmd:DQ_DataQuality>
        <gmd:scope>
          <gmd:DQ_Scope>
            <gmd:level>
              <gmd:MD_ScopeCode codeListValue="dataset"
                                codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_ScopeCode">
                dataset
              </gmd:MD_ScopeCode>
            </gmd:level>
          </gmd:DQ_Scope>
        </gmd:scope>
        <gmd:report>
          <gmd:DQ_DomainConsistency xsi:type="gmd:DQ_DomainConsistency_Type">
            <gmd:result>
              <gmd:DQ_ConformanceResult xsi:type="gmd:DQ_ConformanceResult_Type">
                <gmd:specification>
                  <gmd:CI_Citation>
                    <gmd:title>
                      <gmx:Anchor xlink:href="http://eur-lex.europa.eu/eli/reg/2010/1089">Nariadenie Komisie (EÚ) č.
                        1089/2010 z 23. novembra 2010, ktorým sa vykonáva smernica Európskeho parlamentu a Rady
                        2007/2/ES, pokiaľ ide o interoperabilitu súborov a služieb priestorových údajov
                      </gmx:Anchor>
                    </gmd:title>
                    <gmd:alternateTitle>
                      <gco:CharacterString>Commission Regulation (EU) No 1089/2010 of 23 November 2010 implementing
                        Directive 2007/2/EC of the European Parliament and of the Council as regards interoperability of
                        spatial data sets and services
                      </gco:CharacterString>
                    </gmd:alternateTitle>
                    <gmd:date>
                      <gmd:CI_Date>
                        <gmd:date>
                          <gco:Date>2010-12-08</gco:Date>
                        </gmd:date>
                        <gmd:dateType>
                          <gmd:CI_DateTypeCode
                            codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_DateTypeCode"
                            codeListValue="publication">publication
                          </gmd:CI_DateTypeCode>
                        </gmd:dateType>
                      </gmd:CI_Date>
                    </gmd:date>
                  </gmd:CI_Citation>
                </gmd:specification>
                <gmd:explanation>
                  <gco:CharacterString>Viď Nariadenie Komisie (EÚ) č. 1089/2010</gco:CharacterString>
                </gmd:explanation>
                <gmd:pass>
                  <gco:Boolean>false</gco:Boolean>
                </gmd:pass>
              </gmd:DQ_ConformanceResult>
            </gmd:result>
          </gmd:DQ_DomainConsistency>
        </gmd:report>
        <gmd:lineage>
          <gmd:LI_Lineage>
            <!-- Potrebne doplnit polozku aby bola naplnena, lebo je povinna. Ide o polozku povod dat, ktora popisuje ako vznikli -->
            <!-- ENG: It is necessary to complete the item to be filled, because it is mandatory. This is a data origin item that describes how it originated -->
            <gmd:statement>
              <gco:CharacterString>Územný plán Prešovského samosprávneho kraja (ÚPN PSK) sa spracováva na štátnom mapovom diele a to výkres Širších vzťahov na vektorovej mape Slovenska v mierke 1:200 000 a výkresy Priestorové usporiadanie a funkčné využitie územia, Verejné dopravné vybavenie, Vodné hospodárstvo, Energetika, Ochrana prírody a krajiny, Prvky ÚSES (Územný systém ekologickej stability) na ZM (základná mapa) SR M 1:100 000.</gco:CharacterString>
            </gmd:statement>
          </gmd:LI_Lineage>
        </gmd:lineage>
      </gmd:DQ_DataQuality>
    </gmd:dataQualityInfo>
  </xsl:template>

  <!--Add metadata UUI in identification citation-->
  <xsl:template
    match="/gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/gmd:CI_Citation[count(gmd:identifier) = 0]">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()|comment()"/>
      <gmd:identifier>
        <gmd:MD_Identifier>
          <gmd:code>
            <gco:CharacterString>
              <xsl:value-of select="//gmd:MD_Metadata/gmd:fileIdentifier/gco:CharacterString"></xsl:value-of>
            </gco:CharacterString>
          </gmd:code>
        </gmd:MD_Identifier>
      </gmd:identifier>
    </xsl:copy>
  </xsl:template>

  <!--Add metadata UUI in identification citation-->
  <xsl:template
    match="/gmd:MD_Metadata/gmd:distributionInfo/gmd:MD_Distribution[count(gmd:distributionFormat) = 0]">
    <xsl:copy>
      <gmd:distributionFormat>
        <gmd:MD_Format>
          <gmd:name>
            <gmx:Anchor xlink:href="http://www.iana.org/assignments/media-types/application/gml+xml">gml+xml</gmx:Anchor>
          </gmd:name>
          <gmd:version>
            <gco:CharacterString>3.2</gco:CharacterString>
          </gmd:version>
          <gmd:specification>
            <gmx:Anchor xlink:href="https://inspire.ec.europa.eu/applicationschema/tn">Dopravné siete</gmx:Anchor>
          </gmd:specification>
        </gmd:MD_Format>
      </gmd:distributionFormat>
      <xsl:apply-templates select="@*|node()|comment()"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="//gmd:language/gmd:LanguageCode[@codeListValue = 'slo' or @codeListValue = 'Slovenský']">
    <gmd:LanguageCode codeList="http://www.loc.gov/standards/iso639-2/" codeListValue="slo">
      slo
    </gmd:LanguageCode>
  </xsl:template>

  <!-- Add the 2 inspire decimals ... -->
  <xsl:template match="gmd:EX_GeographicBoundingBox/*/gco:Decimal[matches(., '^(-?[0-9]+|[0-9]+.[0-9]{1})$')]">
    <xsl:copy>
      <xsl:copy-of select="@*"/>
      <xsl:value-of select="format-number(., '#0.00')"/>
    </xsl:copy>
  </xsl:template>


  <!-- Do a copy of every nodes and attributes -->
  <xsl:template match="@*|node()|comment()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()|comment()"/>
    </xsl:copy>

  </xsl:template>

  <!-- Remove geonet:* elements. -->
  <xsl:template match="geonet:*" priority="2"/>

</xsl:stylesheet>
